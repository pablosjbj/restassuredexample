@smoke
Feature: GET

  Scenario: Gets Store endpoint
    Given I perform GET operation for "/store/inventory"
    When The response status code should be "200"
    And The response is not empty
    Then The response should be contains store "sold" like "11"
