@smoke
Feature: GET

  Scenario: Gets Store endpoint
    Given I perform user GET operation for "/user/{username}"
      | username |
      | pablo    |
    When The response status code should be "200"
    And The response is not empty
    Then The response should be contains store "username" like "pablo"
