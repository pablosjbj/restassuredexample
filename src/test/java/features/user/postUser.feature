@smoke
Feature: POST

  Scenario: Post User endpoint
    Given I perform user Post operation for "/user" with body
      | username | firstName | lastName | email           | password | phone   | userStatus |
      | pablo    | ramirez   | zegarra  | pablo@gmail.com | p@ssw0rd | 7321654 | 0          |
    When The response status code should be "200"
    Then The response is not empty

