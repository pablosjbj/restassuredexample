package pojo;

import java.time.LocalDate;

public class Store {

    private int petId;
    private int quantity;
    private String status;
    private boolean complete;
    private LocalDate time;

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setTime(LocalDate dateTime) {
        this.time = dateTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public int getPetId() {
        return petId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getStatus() {
        return status;
    }

    public boolean getComplete() {
        return complete;
    }

    public LocalDate getTime() {
        return time;
    }
}
