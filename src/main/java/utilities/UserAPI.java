package utilities;

import io.restassured.response.Response;
import pojo.Store;
import pojo.User;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

public class UserAPI {

    private IRestClientLibrary restClient;

    public UserAPI() {
        restClient = new RestAssuredExtension();
    }

    public Response PostUser(List<List<String>> data, String url) throws URISyntaxException {
        String username = data.get(1).get(0);
        String firstName = data.get(1).get(1);
        String lastName = data.get(1).get(2);
        String email = data.get(1).get(3);
        String password = data.get(1).get(4);
        String phone = data.get(1).get(5);
        int userStatus = Integer.parseInt(data.get(1).get(6));

        User user = new User();

        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setUserStatus(userStatus);

        return restClient.Post(user, url);
    }

    public Response getByUsername(String url, HashMap<String, String> param) throws URISyntaxException {
        return restClient.GetWithParam(url, param);
    }
}
