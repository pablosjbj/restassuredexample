package utilities;

import io.restassured.response.Response;

import java.net.URISyntaxException;
import java.util.HashMap;

public interface IRestClientLibrary<T> {
    public Response Post(T body, String path) throws URISyntaxException;

    public Response Get(String path) throws URISyntaxException;

    public Response GetWithParam(String path, HashMap<String, String> param) throws URISyntaxException;


}
