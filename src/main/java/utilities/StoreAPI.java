package utilities;

import io.restassured.response.Response;
import pojo.Store;

import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

public class StoreAPI {

    private IRestClientLibrary restClient;

    public StoreAPI() {
        restClient = new RestAssuredExtension();
    }

    public Response GetStoreInventory(String path) throws URISyntaxException {
        return restClient.Get(path);
    }

    public Response PostStore(List<List<String>> data, String endpoint) throws URISyntaxException {
        int petId = Integer.parseInt(data.get(1).get(0));
        int quantity = Integer.parseInt(data.get(1).get(1));
        LocalDate dateTime = LocalDate.now();
        String status = data.get(1).get(2);
        boolean complete = Boolean.parseBoolean(data.get(1).get(3));

        Store store = new Store();
        store.setPetId(petId);
        store.setQuantity(quantity);
        store.setTime(dateTime);
        store.setStatus(status);
        store.setComplete(complete);

        return restClient.Post(store, endpoint);
    }
}
